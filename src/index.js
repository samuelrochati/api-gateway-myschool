const gateway = require("fast-gateway");
const PORT = process.env.PORT || 3000;

gateway({
  middlewares: [require("helmet")()],
  routes: [
    {
      prefix: "/users",
      target: "https://myschool-goftless-api.herokuapp.com/"
    },
    {
      prefix: "/gestor",
      target: "https://api-myschool.herokuapp.com"
    }
  ]
})
  .start(PORT)
  .then(server => {
    console.log(`API Gateway listening on ${PORT} port!`);
  });
